import { Component } from '@angular/core';
import {interval, Subscription, Observable} from 'rxjs'

@Component({
  selector: 'app-todo',
  template: `
    <button type="btn" (click)="stopInterval()" > Stop </button>
    <button type="btn" (click)="startInterval()" > Start </button>
    <p> {{ value }} </p>
  `,
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {

  todos: Array<any> = [];
  deleteTodos: Array<any> = [];
  sub: Subscription;
  private intervalStream$: Observable<number>;
  value;
  constructor() {
    this.sub = new Subscription();
    this.intervalStream$ = interval(1000);
    this.sub = this.intervalStream$.subscribe((value) => {
      this.value = value;
    })
  }

  stopInterval(){
    this.sub.unsubscribe();
  }

  startInterval(){
    this.sub = new Subscription();
    this.intervalStream$ = interval(1000);
    this.sub = this.intervalStream$.subscribe((value) => {
      this.value = value;
    })
  }
}
