import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss']
})
export class TodoComponent {

  todos: Array<any> = [];
  deleteTodos: Array<any> = [];
  

  onAddTodo(todo) {
    this.todos.push(todo);
  }
  onDeleteTodo(todo, index?) {
    delete this.todos[index];
    this.deleteTodos.push(todo);
  }
  onReturnTodo(todo, index?) {
    delete this.deleteTodos[index];
    this.todos[index] = todo;
  }
}
