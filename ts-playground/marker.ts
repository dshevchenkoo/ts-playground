class Marker {
    constructor(private coords: Coords,
                private description: MarkerDescription,
                private time: MarkerTime
                ){};
}

class MarkerDescription implements DescriptionInterface{
    constructor(private name: string, private description: string, private style: string){};
    logDescription() {
        console.log(this.name + ' ' + this.description + ' ' + this.style);
    }
}

interface DescriptionInterface {
    logDescription: () => void;
}

class Coords implements CoordsInterface{
    constructor(private lat: number, private lon: number){};
    clearCoords() {
        this.lat = 0;
        this.lon = 0;
        return this;
    }
}

interface CoordsInterface {
    clearCoords: () => Coords;
}

class MarkerTime implements MarkerTimeInterface{
    constructor(private time: TimeInterface){};
    rewriteDuration(duration: Date) {
        this.time.duration = duration;
        return this;
    }
}

interface MarkerTimeInterface {
    rewriteDuration: (duration: Date) => MarkerTime;
}

interface TimeInterface {
    start: Date
    duration: Date
}